#include "queue_util.h"
#include "unity.h"

// test if items can be pushed to queue until it is filled and if item_count is being updated properly
void test_item_pushed(void) {
  queue_s queue;
  queue__init(&queue);

  for (size_t item = 0; item < queue.max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }
}

void test_item_pushed_2(void) {
  static uint8_t memory[128];
  queue_s queue;
  queue__init_2(&queue, memory, sizeof(memory));

  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }
}

// test if item can be pushed to full queue and if item_count is == max size of queue when full
void test_push_to_full(void) {
  queue_s queue;
  queue__init(&queue);

  for (size_t item = 0; item < queue.max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(queue.max_queue_size, queue__get_item_count(&queue));
}

void test_push_to_full_2(void) {
  static uint8_t memory[128];
  queue_s queue;
  queue__init_2(&queue, memory, sizeof(memory));

  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  TEST_ASSERT_FALSE(queue__push_2(&queue, 123));
  TEST_ASSERT_EQUAL(queue.static_max_queue_size, queue__get_item_count(&queue));
}

// test if all items of full queue can be popped and test if items pushed to queue are in FIFO order by popping them
void test_FIFO(void) {
  queue_s queue;
  queue__init(&queue);

  for (size_t item = 0; item < queue.max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  for (size_t item = 0; item < queue.max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }
}

void test_FIFO_2(void) {
  static uint8_t memory[128];
  queue_s queue;
  queue__init_2(&queue, memory, sizeof(memory));

  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop_2(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }
}

void test_wrap_around(void) {
  queue_s queue;
  queue__init(&queue);

  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
}

void test_wrap_around_2(void) {
  static uint8_t memory[128];
  queue_s queue;
  queue__init_2(&queue, memory, sizeof(memory));

  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push_2(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop_2(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue__pop_2(&queue, &popped_value));
}

// test push until full, pop until empty, then push 0x1A
void test_thorough(void) {
  queue_s queue;
  queue__init(&queue);

  for (size_t item = 0; item < queue.max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }
  for (size_t item = 0; item < queue.max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }
  TEST_ASSERT_TRUE(queue__push(&queue, 0x1A));
}

void test_thorough_2(void) {
  static uint8_t memory[128];
  queue_s queue;
  queue__init_2(&queue, memory, sizeof(memory));

  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }
  for (size_t item = 0; item < queue.static_max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop_2(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }
  TEST_ASSERT_TRUE(queue__push_2(&queue, 0x1A));
}