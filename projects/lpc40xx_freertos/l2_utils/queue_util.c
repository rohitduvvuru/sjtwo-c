#include "queue_util.h"

// This should initialize all members of queue_s
void queue__init(queue_s *queue) {
  queue->push_pointer = 0;
  queue->pop_pointer = 0;
  queue->item_count = 0;
  queue->max_queue_size = sizeof(queue->queue_memory) / sizeof(queue->queue_memory[0]);
}

/* Initialize the queue with user provided static memory
 * @param static_memory_for_queue This memory pointer should not go out of scope
 *
 * @code
 *   static uint8_t memory[128];
 *   queue_s queue;
 *   queue__init(&queue, memory, sizeof(memory));
 * @endcode
 */
void queue__init_2(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  queue->push_pointer = 0;
  queue->pop_pointer = 0;
  queue->item_count = 0;
  queue->static_memory_for_queue = static_memory_for_queue;
  queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
  queue->static_max_queue_size = static_memory_size_in_bytes / sizeof(uint8_t);
}

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value) {
  // if item_count < max_queue_size, not full
  // if not full, push item at push_pointer index, update push_pointer considering wrap-around case, increment
  // item_count, and return true if full, return false
  if (queue->item_count < queue->max_queue_size) {
    queue->queue_memory[queue->push_pointer] = push_value;
    queue->push_pointer = (queue->push_pointer + 1) % (queue->max_queue_size);
    queue->item_count++;
    return true;
  } else {
    return false;
  }
}

/// @returns false if the queue is full
bool queue__push_2(queue_s *queue, uint8_t push_value) {
  if (queue->item_count < queue->static_max_queue_size) {
    queue->static_memory_for_queue[queue->push_pointer] = push_value;
    queue->push_pointer = (queue->push_pointer + 1) % (queue->static_max_queue_size);
    queue->item_count++;
    return true;
  } else {
    return false;
  }
}

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  // if item_count > 0, not empty
  // if not empty, pop item at pop_pointer index, update pop_pointer considering wrap-around case, decrement item_count,
  // and return true if empty, return false
  if (queue->item_count > 0) {
    *pop_value = queue->queue_memory[queue->pop_pointer];
    queue->pop_pointer = (queue->pop_pointer + 1) % (queue->max_queue_size);
    queue->item_count--;
    return true;
  } else {
    return false;
  }
}

/// @returns false if the queue was empty
/// Write the popped value to the user provided pointer pop_value_ptr
bool queue__pop_2(queue_s *queue, uint8_t *pop_value_ptr) {
  if (queue->item_count > 0) {
    *pop_value_ptr = queue->static_memory_for_queue[queue->pop_pointer];
    queue->pop_pointer = (queue->pop_pointer + 1) % (queue->static_max_queue_size);
    queue->item_count--;
    return true;
  } else {
    return false;
  }
}

size_t queue__get_item_count(const queue_s *queue) { return queue->item_count; }