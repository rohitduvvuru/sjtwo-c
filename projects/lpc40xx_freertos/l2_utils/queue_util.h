#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* In this part, the queue memory is statically defined
 * and fixed at compile time for 100 uint8s
 */
typedef struct {
  uint8_t queue_memory[100];
  uint8_t push_pointer;
  uint8_t pop_pointer;
  uint8_t item_count;
  size_t max_queue_size;

  uint8_t *static_memory_for_queue;
  size_t static_memory_size_in_bytes;
  size_t static_max_queue_size;
  // TODO: Add more members as needed
} queue_s;

// This should initialize all members of queue_s
void queue__init(queue_s *queue);

void queue__init_2(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes);

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value);

/// @returns false if the queue is full
bool queue__push_2(queue_s *queue, uint8_t push_value);

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value);

/// @returns false if the queue was empty
/// Write the popped value to the user provided pointer pop_value_ptr
bool queue__pop_2(queue_s *queue, uint8_t *pop_value_ptr);

size_t queue__get_item_count(const queue_s *queue);